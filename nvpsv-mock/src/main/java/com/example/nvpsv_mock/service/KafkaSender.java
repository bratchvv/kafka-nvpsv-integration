package com.example.nvpsv_mock.service;

import com.example.nvpsv_mock.model.NvpsvKafkaRequestMock;
import com.example.nvpsv_mock.model.NvpsvKafkaResponseMock;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

/**
 * Отправитель сообщений в очередь Kafka
 *
 * @author Vladimir Bratchikov
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class KafkaSender {

    private final KafkaTemplate<String, Object> kafkaTemplate; // Бин для отправки сообщений в топик

    @Value("${kafka.topic.smev.out}")// имя топика для отправки сообщений в модуль проверки документов
    private String topicName;

    public void sendData(NvpsvKafkaRequestMock request) {

        NvpsvKafkaResponseMock responseMock = new NvpsvKafkaResponseMock();
        responseMock.setBody(generateTestXml(request.getRequestId()));
        responseMock.setRequestId(request.getRequestId());

        log.info("Отправлен ответ в модуль проверки документов:\n" + responseMock.getBody());
        kafkaTemplate.send(topicName, responseMock);
    }

    /**
     * Сгенерировать тестовый XML от ВИО
     *
     * @param reqId
     * @return
     */
    private static String generateTestXml(String reqId) {
        return "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\">\n"
            + "  <soap:Header>\n"
            + "    <DispatchList>\n"
            + "      <Dispatch>\n"
            + "        <MessageId>8ed9f69dd108479792f518bf1c9e8434</MessageId>\n"
            + "        <CorrelationId>0936771d035f4dcd8e96625cf96a7568</CorrelationId>\n"
            + "        <From>urn:region:777000:UVKIP</From>\n"
            + "        <Recipient>urn:region:777000:NVPSV</Recipient>\n"
            + "        <Task code=\"ZAGS.NVPSV.Inform.Birth.Processing\" version=\"1.0\" status=\"new\">\n"
            + "          <OutputMessage name=\"ZAGS.NVPSV.Inform.Birth.RS\"/>\n"
            + "          <References>\n"
            + "            <ProcessRef code=\"ZAGS.NVPSV.Inform.Birth\" status=\"in_progress\">"+reqId+"</ProcessRef>\n"
            + "          </References>\n"
            + "        </Task>\n"
            + "      </Dispatch>\n"
            + "    </DispatchList>\n"
            + "  </soap:Header>\n"
            + "  <soap:Body>\n"
            + "    <Message>\n"
            + "      <BussinessData contentType=\"application/xml\">\n"
            + "        <tns:informResponse xmlns:tns=\"urn://x-artefacts-zags-inform/root/112-11/4.0.3\"\n"
            + "          xmlns:fnst=\"urn://x-artefacts-zags-inform/types/4.0.3\" ИдЗапрос=\""+reqId+"\"\n"
            + "          ТипЗапрос=\"2\" ТипАГСЗапрос=\"1\" НомерЗаписЗапрос=\"a\" ДатаСвед=\"1957-08-13\" РезОбраб=\"1\" КодСтатус=\"02\">\n"
            + "          <tns:СведРегРожд НомерЗапис=\"111\" ДатаЗапис=\"2020-03-02\">\n"
            + "            <tns:ОрганЗАГС НаимЗАГС=\"a\" КодЗАГС=\"R0000000\"/>\n"
            + "            <tns:СтатусЗаписи ДатаНачСтатус=\"1957-08-13\" КодСтатус=\"04\" НаимСтатус=\"a\"/>\n"
            + "            <tns:СвидетРожд СерияСвидет=\"aaa\" НомерСвидет=\"111\" ДатаВыдСвидет=\"2020-03-02\"/>\n"
            + "            <tns:ПрдСведРег КолРодДетей=\"1\">\n"
            + "              <tns:СведРодившемся ЖивМертв=\"1\" Пол=\"1\">\n"
            + "                <tns:ФИОРожд>\n"
            + "                  <fnst:Фамилия>Иванов</fnst:Фамилия>\n"
            + "                  <fnst:Имя>Иван</fnst:Имя>\n"
            + "                  <fnst:Отчество>Иванович</fnst:Отчество>\n"
            + "                </tns:ФИОРожд>\n"
            + "                <tns:ДатаРожд>2020-03-01</tns:ДатаРожд>\n"
            + "                <tns:МестоРожден МестоТекст=\"РОССИЯ, город Москва\" КодСтраны=\"643\" НаимСтраны=\"РОССИЯ\" Регион=\"77\"\n"
            + "                  НаимСубъект=\"Москва\"/>\n"
            + "              </tns:СведРодившемся>\n"
            + "              <tns:СведМать ОКСМ=\"000\" НаимСтраны=\"a\">\n"
            + "                <fnst:ФИО>\n"
            + "                  <fnst:Фамилия>Иванова</fnst:Фамилия>\n"
            + "                  <fnst:Имя>Инна</fnst:Имя>\n"
            + "                  <fnst:Отчество>Ивановна</fnst:Отчество>\n"
            + "                </fnst:ФИО>\n"
            + "                <fnst:МестоРожден МестоТекст=\"РОССИЯ, город Москва\" КодСтраны=\"643\" НаимСтраны=\"РОССИЯ\" Регион=\"77\"\n"
            + "                  НаимСубъект=\"Москва\"/>\n"
            + "                <fnst:ДатаРожд>1957-08-13</fnst:ДатаРожд>\n"
            + "                <fnst:УдЛичнФЛ КодВидДок=\"21\" НаимДок=\"Паспорт гражданина Российской Федерации\" СерНомДок=\"55\"\n"
            + "                  ДатаДок=\"1957-08-13\" ВыдДок=\"a\" КодВыдДок=\"aaaaaaa\"/>\n"
            + "                <fnst:АдрМЖРФ АдрРФТекст=\"ааа\">\n"
            + "                  <fnst:АдрКЛАДР Индекс=\"111111\" КодРегион=\"01\" НаимРегион=\"Башкортостан\" Город=\"Уфа\" НаселПункт=\"аа\"\n"
            + "                    Улица=\"Ленина\" Дом=\"1\" Корпус=\"1\" Кварт=\"1\"/>\n"
            + "                </fnst:АдрМЖРФ>\n"
            + "              </tns:СведМать>\n"
            + "              <tns:СведОтец ОКСМ=\"000\" НаимСтраны=\"a\">\n"
            + "                <fnst:ФИО>\n"
            + "                  <fnst:Фамилия>Иванов</fnst:Фамилия>\n"
            + "                  <fnst:Имя>Иван</fnst:Имя>\n"
            + "                  <fnst:Отчество>Иванович</fnst:Отчество>\n"
            + "                </fnst:ФИО>\n"
            + "                <fnst:МестоРожден МестоТекст=\"РОССИЯ, город Москва\" КодСтраны=\"643\" НаимСтраны=\"РОССИЯ\" Регион=\"77\"\n"
            + "                  НаимСубъект=\"Москва\"/>\n"
            + "                <fnst:ДатаРожд>1957-08-13</fnst:ДатаРожд>\n"
            + "                <fnst:УдЛичнФЛ КодВидДок=\"21\" НаимДок=\"Паспорт гражданина Российской Федерации\" СерНомДок=\"55\"\n"
            + "                  ДатаДок=\"1957-08-13\" ВыдДок=\"a\" КодВыдДок=\"aaaaaaa\"/>\n"
            + "                <fnst:АдрМЖРФ АдрРФТекст=\"ааа\">\n"
            + "                  <fnst:АдрКЛАДР Индекс=\"111111\" КодРегион=\"01\" НаимРегион=\"Башкортостан\" Город=\"Уфа\" НаселПункт=\"аа\"\n"
            + "                    Улица=\"Ленина\" Дом=\"1\" Корпус=\"1\" Кварт=\"1\"/>\n"
            + "                </fnst:АдрМЖРФ>\n"
            + "              </tns:СведОтец>\n"
            + "              <tns:СведДокРод КодДок=\"55555\" НаимДок=\"Паспорт гражданина Российской Федерации\" СерНомДок=\"аааа\"\n"
            + "                ДатаДок=\"2017-08-13\">\n"
            + "                <tns:НаимОрг>ааа</tns:НаимОрг>\n"
            + "              </tns:СведДокРод>\n"
            + "            </tns:ПрдСведРег>\n"
            + "          </tns:СведРегРожд>\n"
            + "        </tns:informResponse>\n"
            + "      </BussinessData>\n"
            + "    </Message>\n"
            + "  </soap:Body>\n"
            + "</soap:Envelope>\n";
    }
}
