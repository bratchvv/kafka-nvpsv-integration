package com.example.nvpsv_mock.service;

import com.example.nvpsv_mock.model.NvpsvKafkaRequestMock;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

/**
 * Слушатель сообщений из очереди Kafka
 *
 * @author Vladimir Bratchikov
 */
@Service
@Slf4j
@RequiredArgsConstructor
public class KafkaReceiver {

    private final KafkaSender kafkaSender;

    @KafkaListener(topics = "${kafka.topic.smev.in}", containerGroup = "${kafka.consumer.group.id}",
        containerFactory = "kafkaContainerFactory")
    public void receiveData(NvpsvKafkaRequestMock requestMock) {
        log.info("Получен запрос от модуля проверки документов:\n" + requestMock.getBody());
        kafkaSender.sendData(requestMock);
    }
}
