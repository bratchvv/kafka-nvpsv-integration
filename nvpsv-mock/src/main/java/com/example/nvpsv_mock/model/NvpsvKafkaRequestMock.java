package com.example.nvpsv_mock.model;


import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author Vladimir Bratchikov
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class NvpsvKafkaRequestMock implements Serializable {

    private String queue;
    private String jmsReplyTo;
    private String requestId;
    private String body;

}
