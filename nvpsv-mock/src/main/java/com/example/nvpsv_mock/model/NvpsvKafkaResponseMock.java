package com.example.nvpsv_mock.model;

import java.io.Serializable;

import lombok.Data;

/**
 * Ответ из топика кафки. В нем есть requestId и тело XML ответа из ВИО
 *
 * @author Vladimir Bratchikov
 */
@Data
public class NvpsvKafkaResponseMock implements Serializable {

    private String requestId;// На всякий случай, д б без дефисов (это прикол у ВИО такой)
    private String body;// Строка с XML от ВИО
}
