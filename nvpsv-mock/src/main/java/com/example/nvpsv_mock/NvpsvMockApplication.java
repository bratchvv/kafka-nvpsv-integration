package com.example.nvpsv_mock;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Приложение которое эмулирует работу НВПиСВ.
 * А именно, прием сообщения из кафки от модуля проверки документов,
 * потом мочит ответ от вио и возвращает результат обратно в кафку в модуль проверки документов.
 */
@SpringBootApplication
@RequiredArgsConstructor
public class NvpsvMockApplication {

    public static void main(String[] args) {
        SpringApplication.run(NvpsvMockApplication.class, args);
    }
}
