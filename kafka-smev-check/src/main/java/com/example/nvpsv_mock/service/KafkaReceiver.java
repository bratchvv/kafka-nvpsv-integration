package com.example.nvpsv_mock.service;

import com.example.nvpsv_mock.model.NvpsvKafkaResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

/**
 * Слушатель сообщений из очереди Kafka
 *
 * @author Vladimir Bratchikov
 */
@Service
@Slf4j
public class KafkaReceiver {

    @KafkaListener(topics = "${kafka.topic.smev.out}", containerGroup = "${kafka.consumer.group.id}",
        containerFactory = "kafkaContainerFactory")
    public void receiveData(NvpsvKafkaResponse response) {
        log.info("Получен ответ от ВИО: " + response.getBody());
    }
}
