package com.example.nvpsv_mock.service;

import com.example.nvpsv_mock.model.NvpsvKafkaRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

/**
 * Отправитель сообщений в очередь Kafka
 *
 * @author Vladimir Bratchikov
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class KafkaSender {

    private final KafkaTemplate<String, Object> kafkaTemplate; // Бин для отправки сообщений в топик

    @Value("${kafka.topic.smev.in}")// имя топика для отправки сообщений в НВПиСВ
    private String topicName;

    public void sendData(NvpsvKafkaRequest request) {
        log.info("Отправлен запрос в ВИО:, {}", request.getBody());
        kafkaTemplate.send(topicName, request);
    }
}
