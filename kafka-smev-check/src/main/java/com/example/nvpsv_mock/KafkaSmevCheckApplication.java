package com.example.nvpsv_mock;

import static java.lang.Thread.sleep;

import com.example.nvpsv_mock.model.NvpsvKafkaRequest;
import com.example.nvpsv_mock.service.KafkaSender;
import java.util.UUID;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;


/**
 * Приложение которое эмулирует работу модуля проверки документов.
 * А именно, отправку сообщения в кафки,
 * потом прием ответа от НВПиСВ.
 */
@SpringBootApplication
@RequiredArgsConstructor
public class KafkaSmevCheckApplication {

	private final KafkaSender kafkaSender;
	public static final int COUNT = 1;

	public static void main(String[] args) {
		SpringApplication.run(KafkaSmevCheckApplication.class, args);
	}

	/**
	 * Просто отправляем тестовый запрос при запуске.
	 */
	@SneakyThrows
	@EventListener(ApplicationReadyEvent.class)
	public void run() {
		sleep(5000);
		for (int i = 0; i < COUNT; i++) {

			String queue = "DEV.QUEUE.6"; // очередь JMS, на ВИО это ADP.NVPSV.ROUTING.OUT, например
			String jmsReplyTo = null; // очередь JMS для ответа
			String requestId = UUID.randomUUID().toString().replace("-", ""); //requestID(без дефисов!)
			String body = generateTestXml(requestId); // тело XML

			NvpsvKafkaRequest request = new NvpsvKafkaRequest(queue, jmsReplyTo, requestId, body);

			sleep(1000);
			kafkaSender.sendData(request);
		}
	}

	/**
	 * Сгенерировать тестовый XML в ВИО
	 *
	 * @param reqId
	 * @return
	 */
	private static String generateTestXml(String reqId) {
		return "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
			+ "<ns2:Envelope xmlns:ns2=\"http://www.w3.org/2003/05/soap-envelope\">\n"
			+ "  <ns2:Header>\n"
			+ "    <DispatchList>\n"
			+ "      <Dispatch>\n"
			+ "        <MessageId>2c4d9772039149eda081cf1e66735b12</MessageId>\n"
			+ "        <From>urn:region:777000:NVPSV</From>\n"
			+ "        <Recipient>urn:region:777000:UVKIP</Recipient>\n"
			+ "        <Task code=\"ZAGS.NVPSV.Inform.Birth.Processing\" version=\"1.0\" status=\"new\">\n"
			+ "          <InputMessage name=\"ZAGS.NVPSV.Inform.Birth.RQ\"/>\n"
			+ "          <References>\n"
			+ "            <ProcessRef code=\"ZAGS.NVPSV.Inform.Birth\" "
			+ "status=\"in_progress\">" + reqId + "</ProcessRef>\n"
			+ "          </References>\n"
			+ "        </Task>\n"
			+ "      </Dispatch>\n"
			+ "    </DispatchList>\n"
			+ "  </ns2:Header>\n"
			+ "  <ns2:Body>\n"
			+ "    <Message>\n"
			+ "      <BussinessData contentType=\"application/xml\">\n"
			+ "        <informRequest ИдЗапрос=\"ca0c074c7d694c7b9a831da7e26a972f\" ТипАГС=\"1\" ТипЗапрос=\"2\"\n"
			+ "          xmlns=\"urn://x-artefacts-zags-inform/root/112-11/4.0.3\" xmlns:ns2=\"urn://x-artefacts-zags-inform/types/4.0.3\">\n"
			+ "          <СведФЛ>\n"
			+ "            <ФИО>\n"
			+ "              <ns2:Фамилия>1</ns2:Фамилия>\n"
			+ "              <ns2:Имя>2</ns2:Имя>\n"
			+ "              <ns2:Отчество>3</ns2:Отчество>\n"
			+ "            </ФИО>\n"
			+ "            <ДатаРожд>2000-11-11</ДатаРожд>\n"
			+ "          </СведФЛ>\n"
			+ "        </informRequest>\n"
			+ "      </BussinessData>\n"
			+ "    </Message>\n"
			+ "  </ns2:Body>\n"
			+ "</ns2:Envelope>\n";
	}
}
