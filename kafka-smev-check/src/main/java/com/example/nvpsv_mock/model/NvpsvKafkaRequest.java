package com.example.nvpsv_mock.model;


import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author Vladimir Bratchikov
 */
@Data
@AllArgsConstructor
public class NvpsvKafkaRequest implements Serializable {

    private String queue;
    private String jmsReplyTo;
    private String requestId;
    private String body;

}
