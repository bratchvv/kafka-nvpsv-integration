package com.example.nvpsv_mock.config;

import com.example.nvpsv_mock.model.NvpsvKafkaResponse;
import java.util.HashMap;
import java.util.Map;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;

/**
 * Конфиг Kafka Consumer
 *
 * @author Vladimir Bratchikov
 */
@Configuration
@EnableKafka
public class KafkaConsumerConfig {

    @Value("${kafka.bootstrap-servers}")
    private String kafkaServer;

    @Value("${kafka.consumer.group.id}")
    private String kafkaGroupId;

    @Bean
    public ConsumerFactory<String, NvpsvKafkaResponse> kafkaConsumerFactory() {
        Map<String, Object> config = new HashMap<>();
        config.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaServer);
        config.put(ConsumerConfig.GROUP_ID_CONFIG, kafkaGroupId);
        config.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        config.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
        config.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);
        config.put(ConsumerConfig.AUTO_COMMIT_INTERVAL_MS_CONFIG, 1000);
        config.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, 8);
        config.put(ConsumerConfig.MAX_POLL_INTERVAL_MS_CONFIG, 10000);
        config.put(ConsumerConfig.SESSION_TIMEOUT_MS_CONFIG, 30000);
        config.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        config.put(ConsumerConfig.ISOLATION_LEVEL_CONFIG, "read_committed");
        config.put(JsonDeserializer.TRUSTED_PACKAGES, "*");
        return new DefaultKafkaConsumerFactory<>(config, null,
                                                 new JsonDeserializer(NvpsvKafkaResponse.class,
                                                                      false)); // false чтоб не ругалось на название класса
    }


    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, NvpsvKafkaResponse> kafkaContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, NvpsvKafkaResponse> factory = kafkaListenerContainerFactory();
        factory.setConsumerFactory(kafkaConsumerFactory());
        factory.setConcurrency(8);
        factory.setBatchListener(true);
        return factory;
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, NvpsvKafkaResponse> kafkaListenerContainerFactory() {
        ConcurrentKafkaListenerContainerFactory<String, NvpsvKafkaResponse> listener = new ConcurrentKafkaListenerContainerFactory<>();
        listener.setConsumerFactory(kafkaConsumerFactory());
        return listener;
    }
}
